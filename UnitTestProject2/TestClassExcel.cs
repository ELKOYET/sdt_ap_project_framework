﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop;


namespace AirEmissionTest
{
    class TestClass

    {
        public TestClass(string jopa)
        {
            ExcelRead(jopa);
        }
        public double[] Testdatacoef;

        public double[] Testdatafirst;
        public double[] r_coef;
        public double[] Xmu_coef;
        public double[] Cmv_coef;
        public double[] s1_coef;
        public double[] cx_coef;
        public double[] cy_coef;
        public double[,] CY = new double[5,5];


        public void ExcelRead(string filename)
        {
            Application ex = new Application();
            Workbook exBook = ex.Workbooks.OpenXML(AppDomain.CurrentDomain.BaseDirectory + @"\" + filename);
            Worksheet exSheets;
            exSheets = (Worksheet)exBook.Sheets[1];
            Microsoft.Office.Interop.Excel.Range resultcoef = exSheets.UsedRange.Range[exSheets.Cells[13, 8], exSheets.Cells[21, 8]] as Microsoft.Office.Interop.Excel.Range;
            Microsoft.Office.Interop.Excel.Range resultfirst = exSheets.UsedRange.Range[exSheets.Cells[15, 3], exSheets.Cells[17, 3]] as Microsoft.Office.Interop.Excel.Range;


            Array resultcoefarray = (Array)resultcoef.Cells.Value2;
            Testdatacoef = resultcoefarray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            Array resultfirstarray = (Array)resultfirst.Cells.Value2;
            Testdatafirst = resultfirstarray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();
            // r_coef exceldata
            Microsoft.Office.Interop.Excel.Range resulR_coef = exSheets.UsedRange.Range[exSheets.Cells[23, 2], exSheets.Cells[23, 5]] as Microsoft.Office.Interop.Excel.Range;
            Array resultRarray = (Array)resulR_coef.Cells.Value2;
            r_coef = resultRarray.OfType<object>().Select(o => Math.Round(Convert.ToDouble(o), 3)).ToArray();
            // Xmu exceldata
            Microsoft.Office.Interop.Excel.Range resulXMU_coef = exSheets.UsedRange.Range[exSheets.Cells[27, 2], exSheets.Cells[27, 5]] as Microsoft.Office.Interop.Excel.Range;
            Array resulXMU_coefarray = (Array)resulXMU_coef.Cells.Value2;
            Xmu_coef = resulXMU_coefarray.OfType<object>().Select(o => Math.Round(Convert.ToDouble(o), 0)).ToArray();
            // Cmv exceldata
            Microsoft.Office.Interop.Excel.Range resulCMV_coef = exSheets.UsedRange.Range[exSheets.Cells[25, 2], exSheets.Cells[25, 5]] as Microsoft.Office.Interop.Excel.Range;
            Array resulCMV_coefarray = (Array)resulCMV_coef.Cells.Value2;
            Cmv_coef = resulCMV_coefarray.OfType<object>().Select(o => Math.Round(Convert.ToDouble(o), 4)).ToArray();
            // S1_coef exceldata
            Microsoft.Office.Interop.Excel.Range resuls1_coef = exSheets.UsedRange.Range[exSheets.Cells[30, 2], exSheets.Cells[30, 6]] as Microsoft.Office.Interop.Excel.Range;
            Array resuls1_coefarray = (Array)resuls1_coef.Cells.Value2;
            s1_coef = resuls1_coefarray.OfType<object>().Select(o => Math.Round(Convert.ToDouble(o), 2)).ToArray();
            // Cx exceldata
            Microsoft.Office.Interop.Excel.Range resulcx_coef = exSheets.UsedRange.Range[exSheets.Cells[29, 2], exSheets.Cells[29, 6]] as Microsoft.Office.Interop.Excel.Range;
            Array resulcx_coefarray = (Array)resulcx_coef.Cells.Value2;
            cx_coef = resulcx_coefarray.OfType<object>().Select(o => Math.Round(Convert.ToDouble(o), 3)).ToArray();

            // Cy exceldata
            Microsoft.Office.Interop.Excel.Range resulcy_coef = exSheets.UsedRange.Range[exSheets.Cells[32, 2], exSheets.Cells[36, 6]] as Microsoft.Office.Interop.Excel.Range;
            Array resulcy_coefarray = (Array)resulcy_coef.Cells.Value2;
            cy_coef = resulcy_coefarray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();
            ex.Workbooks.Close();



        }
        //Преобразование одномерного массива данных из екселя в двумерных для тестов
        int k = 0;
        public double[,]  HU() 
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++) 
                {
                    
                    CY[j, i] = cy_coef[k++];
                }
                if (k == 25) 
                {
                    k = 0;
                }
                    
            }
            return CY;


        }
        
    }
}
