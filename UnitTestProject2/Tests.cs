﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AirPollutionLib;
using AirEmissionTest;

namespace AirEmissionTest
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestMethod1()
        {

            string filename = "KURSACH.xlsx";
            ClassAirPollutionInput AirPollutionInput = new ClassAirPollutionInput(80, 6.4, 100, 30, 0.1, 1198800, 160, 75);
            ClassAirPollutionCount AirPollutionCount = new ClassAirPollutionCount();
            AirPollutionCount.AirPollutionInput = AirPollutionInput;
            

            TestClass test = new TestClass(filename);

            AirPollutionInput.H = 80;
            AirPollutionInput.D = 6.4;
            AirPollutionInput.Tr = 100;
            AirPollutionInput.Ta = 30;
            AirPollutionInput.Zo = 0.1;
            AirPollutionInput.V = 1198800;
            AirPollutionInput.A = 160;
            AirPollutionInput.nu = 75;
            AirPollutionInput.E = 0;
            AirPollutionInput.Velocity = 1;
          

            Assert.AreEqual(Math.Round(test.Testdatafirst[1], 4), AirPollutionCount.Wo_AverageVelocityOutfall());
            Assert.AreEqual(Math.Round(test.Testdatafirst[0], 0), AirPollutionCount.V1_MixtureVolume());
            Assert.AreEqual(Math.Round(test.Testdatafirst[2], 1), AirPollutionCount.M_DustAmount());
            Assert.AreEqual(Math.Round(test.Testdatacoef[0], 3), AirPollutionCount.f_Coefficient());
            Assert.AreEqual(Math.Round(test.Testdatacoef[1], 2), AirPollutionCount.m_Coefficient());
            Assert.AreEqual(Math.Round(test.Testdatacoef[2], 2), AirPollutionCount.Vm_Coefficient());
            Assert.AreEqual(Math.Round(test.Testdatacoef[3], 4), AirPollutionCount.n_Coefficient());
            Assert.AreEqual(Math.Round(test.Testdatacoef[5], 4), AirPollutionCount.F_Coefficient());
            Assert.AreEqual(Math.Round(test.Testdatacoef[6], 2), AirPollutionCount.Cm_MaxPollutionConcentration());
            Assert.AreEqual(Math.Round(test.Testdatacoef[4], 2), AirPollutionCount.d_Coefficient());
            Assert.AreEqual(Math.Round(test.Testdatacoef[8], 0), AirPollutionCount.Xm_TorchDistance());
            Assert.AreEqual(Math.Round(test.Testdatacoef[7], 2), AirPollutionCount.um_DangerousWindVelocity());
            CollectionAssert.AreEqual(test.r_coef, AirPollutionCount.r_CoefficientCount());          
            CollectionAssert.AreEqual(test.Xmu_coef, AirPollutionCount.Xmu_MaxConcentrationDistanceCount());
            CollectionAssert.AreEqual(test.s1_coef, AirPollutionCount.S1_CoefficientCount());
            CollectionAssert.AreEqual(test.cx_coef, AirPollutionCount.Cx_ConcentrationCountOfDistanceCount());

     

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++) 
                {
                    Assert.AreEqual(test.HU()[i, j].ToString("#.##"), AirPollutionCount.Cy_ConcentrationCountOfDistanceCount()[i, j].ToString("#.##"));
                }
            }
           


        }
    }
}
