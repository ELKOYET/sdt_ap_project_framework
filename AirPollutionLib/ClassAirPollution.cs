﻿using System;

namespace AirPollutionLib
{
    //Собственно класс входных значений
    public class ClassAirPollutionInput
    {
       public  ClassAirPollutionInput() 
        { }

        public ClassAirPollutionInput(double _H, double _D, int _Tr, int _Ta, double _Zo, double _V, int _A, int _nu) 
        {
            H = _H;
            D = _D;
            Tr = _Tr;
            Ta = _Ta;
            Zo = _Zo;
            V = _V;
            A = _A;
            nu = _nu;
            
        }
        public double H { get; set; }
        public double D { get; set; }
        public int Tr { get; set; }
        public int Ta { get; set; }
        public double Zo { get; set; }
        public double V { get; set; }
        public int A { get; set; }
        public int nu { get; set; }

        public int E;
        public double Velocity;
    }
    //и класс выходных само собой имеется:
    public class ClassAirPollutionCount
    {

       
        public ClassAirPollutionInput AirPollutionInput;
        //массив расстояний по Х
        public double[] X = new double[5] { 1000, 3000, 5000, 10000, 15000 };
        //массив скоростей
        public double[] u = new double[4] { 1, 2, 4, 6 };
        //массив расстояний по У
        public double[] Y = new double[5] { 100, 200, 300, 400, 500 };
       
        private double n;
        private double d;
        private double Xm;
        private double um;
        private double f;

       //Далее тысяча и один метод для расчета 
        public double Wo_AverageVelocityOutfall()
        {
            return Math.Round(4 * (AirPollutionInput.V / 3600) / (3.14 * Math.Pow(AirPollutionInput.D, 2)), 4);
        }
       
        public double V1_MixtureVolume()
        {
            return Math.Round(3.14 * Math.Pow(AirPollutionInput.D, 2) * Wo_AverageVelocityOutfall() / 4, 0);
        }
      
        public double M_DustAmount()
        {
            return Math.Round(AirPollutionInput.Zo * V1_MixtureVolume(), 1);
        }
        public double f_Coefficient()
        {

            return Math.Round((AirPollutionInput.D * (Math.Pow(Wo_AverageVelocityOutfall(), 2) * 1000)) / (Math.Pow(AirPollutionInput.H, 2) * (AirPollutionInput.Tr - AirPollutionInput.Ta)), 3);
        }
        public double m_Coefficient()
        {
            return Math.Round(Math.Pow(0.67 + 0.1 * Math.Sqrt(f_Coefficient()) + 0.34 * Math.Sqrt(f_Coefficient()), -1), 2);
        }
        public double Vm_Coefficient()
        {
            return Math.Round((0.65 * Math.Pow(V1_MixtureVolume() * (AirPollutionInput.Tr - AirPollutionInput.Ta) / AirPollutionInput.H, 1.0 / 3.0)), 2);
        }

       

        public double n_Coefficient()
        {
            if (Vm_Coefficient() <= 0.3)
            {
                n = 3;
            }
            if (Vm_Coefficient() > 0.3 & Vm_Coefficient() <= 2)
            {
                n = Math.Round(3 - Math.Sqrt((Vm_Coefficient() - 0.3) * (4.36 - Vm_Coefficient())), 3);
            }
            if (Vm_Coefficient() > 2)
            {
                n = 1;
            }
            return n;
        }

       
        public double F_Coefficient()
        {
            if (AirPollutionInput.E == 1)
            {
                f = 1;
            }
            if (AirPollutionInput.E == 0 && AirPollutionInput.nu >= 90)
            {
                f = 2;
            }
            if (AirPollutionInput.E == 0 && AirPollutionInput.nu < 75)
            {
                f = 3;
            }
            if (AirPollutionInput.E == 0 && AirPollutionInput.nu >= 75 && AirPollutionInput.nu < 90)
            {
                f = 2.5;
            }
            return f;
        }
        
        public double Cm_MaxPollutionConcentration()
        {
            return Math.Round((AirPollutionInput.A * M_DustAmount() * F_Coefficient() * m_Coefficient() * n_Coefficient()) / (Math.Pow(AirPollutionInput.H, 2) * Math.Pow(V1_MixtureVolume() * (AirPollutionInput.Tr - AirPollutionInput.Ta), 1.0 / 3.0)), 2);
        }

        

        public double d_Coefficient()
        {
            if (Vm_Coefficient() <= 2)
            {
                d = 4.95 * Vm_Coefficient() * (1 + 0.28 * Math.Pow(f_Coefficient(), 1.0 / 3.0));
            }
            if (Vm_Coefficient() > 2)
            {
                d = 7 * Math.Sqrt(Vm_Coefficient()) * (1 + 0.28 * Math.Pow(f_Coefficient(), 1.0 / 3.0));
            }
            return Math.Round(d, 2);
        }

        
        public double Xm_TorchDistance()
        {
            if (F_Coefficient() >= 2)
            {
                Xm = AirPollutionInput.H * d_Coefficient() * ((5 - F_Coefficient()) / 4);
            }
            if (F_Coefficient() < 2)
            {
                Xm = AirPollutionInput.H * d_Coefficient();
            }
            return Xm;
        }


      
        public double um_DangerousWindVelocity()
        {
            if (Vm_Coefficient() > 2)
            {
                um = Math.Round(Vm_Coefficient() * (1 + 0.12 * Math.Sqrt(f_Coefficient())), 2);
            }
            if (Vm_Coefficient() > 0.5 & Vm_Coefficient() <= 2)
            {
                um = Vm_Coefficient();
            }
            if (Vm_Coefficient() <= 0.5)
            {
                um = Math.Round(0.5 * Vm_Coefficient(), 2);
            }
            return um;
        }

        

        public double[] r_CoefficientArray = new double[4];
        public double[] r_CoefficientCount()
        {
            for (int i = 0; i < r_CoefficientArray.Length; i++)
            {
                if ((u[i] / um_DangerousWindVelocity()) < 1)
                {
                    r_CoefficientArray[i] = Math.Round(0.67 * (u[i] / um_DangerousWindVelocity()) + 1.67 * Math.Pow(u[i] / um_DangerousWindVelocity(), 2) - 1.34 * Math.Pow(u[i] / um_DangerousWindVelocity(), 3), 3);
                }
                if ((u[i] / um_DangerousWindVelocity()) > 1)
                {
                    r_CoefficientArray[i] = Math.Round(3 * (u[i] / um_DangerousWindVelocity()) / (2 * Math.Pow((u[i] / um_DangerousWindVelocity()), 2) - (u[i] / um_DangerousWindVelocity()) + 2), 3);
                }
            }
            return r_CoefficientArray;
        }

        

        public double[] Cmv_MaxPollutionConcetrationVelocityArray = new double[4];
        public double[] Cmv_MaxPolutionConcentrationVelocityCount()
        {
            for (int i = 0; i < Cmv_MaxPollutionConcetrationVelocityArray.Length; i++)
            {
                Cmv_MaxPollutionConcetrationVelocityArray[i] = Math.Round( r_CoefficientCount()[i] * Cm_MaxPollutionConcentration(),4);
            }
            return Cmv_MaxPollutionConcetrationVelocityArray;
        }

        

        public double[] Xmu_MaxConcentrationDistanceArray = new double[4];
        public double[] Xmu_MaxConcentrationDistanceCount()
        {
            for (int i = 0; i < Xmu_MaxConcentrationDistanceArray.Length; i++)
            {
                if ((u[i] / um) < 0.25)
                {
                    Xmu_MaxConcentrationDistanceArray[i] =  Math.Round( 3 * Xm_TorchDistance(),0);
                }
                if ((u[i] / um) < 1 && (u[i] / um) >= 0.25)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round( (8.43 * Math.Pow(1 - (u[i] / um_DangerousWindVelocity()), 5) + 1) * Xm_TorchDistance(),0);
                }
                if ((u[i] / um) > 1)
                {
                    Xmu_MaxConcentrationDistanceArray[i] = Math.Round( (0.32 * (u[i] / um_DangerousWindVelocity()) + 0.68) * Xm_TorchDistance(),0);
                }
            }
            return Xmu_MaxConcentrationDistanceArray;
        }
        public double[] S1_CoefficientArray = new double[5];
        public double[] S1_CoefficientCount() 
        {
            for (int i = 0; i < S1_CoefficientArray.Length; i++)
            {
                if ((X[i] / Xm_TorchDistance()) <= 1) 
                {
                    S1_CoefficientArray[i] = Math.Round( 3 * Math.Pow((X[i] / Xm_TorchDistance()), 4) - 8 * Math.Pow((X[i] / Xm_TorchDistance()), 3) + 6 * Math.Pow((X[i] / Xm_TorchDistance()), 2),2);
                }
                if ((X[i] / Xm_TorchDistance()) <= 8 && (X[i] / Xm_TorchDistance()) > 1) 
                {
                    S1_CoefficientArray[i] = Math.Round( 1.13 / (0.13 * Math.Pow( (X[i] / Xm_TorchDistance()), 2) + 1),2);
                }
                if ((X[i] / Xm_TorchDistance()) > 8) 
                {
                    S1_CoefficientArray[i] = Math.Round( Math.Pow(0.1* Math.Pow((X[i] / Xm_TorchDistance()),2)+ 2.47*(X[i]/Xm_TorchDistance())-17.8 ,-1),2);
                }

            }
            return S1_CoefficientArray;
        }
        public double[] Cx_ConcentrationCountOfDistanceArray = new double[5];
        public double[] Cx_ConcentrationCountOfDistanceCount() 
        {
            for (int i = 0; i < Cx_ConcentrationCountOfDistanceArray.Length; i++) 
            {
                Cx_ConcentrationCountOfDistanceArray[i] = Math.Round( S1_CoefficientCount()[i] * Cm_MaxPollutionConcentration(),3);
            }
            return Cx_ConcentrationCountOfDistanceArray;
        }
        public double[,] S2_CoefficientArray = new double[5, 5];
        
        public double[,] S2_CoefficientCount() 
        {
            for (int i = 0; i < 5; i++) 
            {
                for (int j = 0; j< 5; j++) 
                {
                    S2_CoefficientArray[i, j] = Math.Pow( (1d + 8.4d * AirPollutionInput.Velocity * Math.Pow( (Y[i] / X[j]),2))*(1d+28.2d*Math.Pow(u[0],2)* Math.Pow((Y[i]/X[j]),4)),-1);
                }
            }
            return S2_CoefficientArray;
        }

        public double[,] Cy_ConcentrationCountOfDistanceArray = new double[5, 5];
        public double[,] Cy_ConcentrationCountOfDistanceCount()
        {
           
            for (int i = 0; i < 5; i++) 
            {
                for (int j = 0; j < 5; j++) 
                {
                    Cy_ConcentrationCountOfDistanceArray[j, i] = S2_CoefficientCount()[i, j] * Cx_ConcentrationCountOfDistanceCount()[j];
                }
            }
            return Cy_ConcentrationCountOfDistanceArray;
        }

    }
}