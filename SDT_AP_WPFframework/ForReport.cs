﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDT_AP_WPFframework
{

    public class ForReport
    {
        public ForReport() { }

        public ForReport(string _name, string _value)
        {
            Name = _name;
            Value = _value;
        }


        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
       
        public string this[int ind]
        {
            get
            {
                switch (ind)
                {
                    case 1:
                        return _name;
                    case 2:
                        return _value;
                    default:
                        return "null";
                }
            }
            set
            {
                switch (ind)
                {
                    case 1:
                        _name = value;
                        break;
                    case 2:
                        _value = value;
                        break;
                    default:
                        break;
                }
            }
        }



    }
    // Два класса для формирования отчета(сделано по заповедям В.В.Лаврова)
    class InputForReport : ForReport
    {
    }
    class OutPutForReport : ForReport 
    { 
    }
}
