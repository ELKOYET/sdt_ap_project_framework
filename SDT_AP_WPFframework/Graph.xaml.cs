﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms.DataVisualization;
using System.Windows.Forms.DataVisualization.Charting;
using AirPollutionLib;
using LiveCharts.Wpf.Charts.Base;

namespace SDT_AP_WPFframework
{
    /// <summary>
    /// Логика взаимодействия для Graph.xaml
    /// </summary>
    public partial class Graph : Window
    {
        ClassAirPollutionCount count;
       
       
        public Graph(ClassAirPollutionCount count )
        {
            InitializeComponent();
            this.count = count;
            SliderMin.Value = 0;
            SliderMax.Value = 15000;

            CreateGraph();
        }
        //Создание графика
        public void CreateGraph()
        {
          

            Chart.ChartAreas.Clear();
            Chart.Series.Clear();
            
           

            //Опять таки из-за WinHost пришлось толщину и шрифт задавать вручную
            Chart.ChartAreas.Add(new ChartArea("Series"));
            Chart.Series.Add(new Series("CY"));
            Chart.Series["CY"].ChartArea = "Series";
            Chart.Series["CY"].ChartType = SeriesChartType.Spline;
            Chart.ChartAreas["Series"].Axes[0].Title = "Расстояние по оси Х,м";
            Chart.ChartAreas["Series"].Axes[1].Title = "Величина приземной концентрации,мг/м3";
            Chart.ChartAreas["Series"].AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            Chart.ChartAreas["Series"].AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            Chart.Series["CY"].BorderWidth = 4;
            Chart.ChartAreas["Series"].AxisX.Minimum =  Convert.ToDouble(Math.Round( SliderMin.Value,0));
            Chart.ChartAreas["Series"].AxisX.Maximum =  Convert.ToDouble(Math.Round( SliderMax.Value,0));
            int j  = ComboY.SelectedIndex;

            for (int i = 0; i <= count.Xmu_MaxConcentrationDistanceCount().Length; i++)
            {
                double axisX = count.X[i];
                double axisY = count.Cy_ConcentrationCountOfDistanceCount()[i, j];
                DataPoint dt = new DataPoint(axisX, axisY);
                Chart.Series["CY"].Points.Add(dt);
            }


        }

        
      

        private void ComboY_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (Chart.Series.Count == 1)
            {
                CreateGraph();
            }
        }

        #region Масштабирование графика
        private void SliderMin_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(Chart.Series.Count == 1)
            {
                CreateGraph();
            }
           
        }

        private void SliderMax_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Chart.Series.Count == 1)
            {
                CreateGraph();
            }
        }
        #endregion
    }


}
