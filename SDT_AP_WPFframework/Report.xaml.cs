﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WebForms;
using System.Data;
using AirPollutionLib;
using System.Windows.Forms;
using SDT_AP_WPFframework;

namespace SDT_AP_WPFframework
{
    /// <summary>
    /// Логика взаимодействия для Report.xaml
    /// </summary>
    public partial class Report : Window
    {
        private System.ComponentModel.IContainer components = null;
        public System.Windows.Forms.BindingSource InputBinding;
        public System.Windows.Forms.BindingSource OutputBunding;

       
        public Report()
        {
            

            InitializeComponent();
                
            //Так как при интеграции отчета через Winhosts невозможно посмотреть на Report до открытия окна,мне пришлось почти полностью переписать сюда дизайнер форм
            //Я знаю,что можно было сделать проще,но я захотел делать так
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();

            this.InputBinding = new System.Windows.Forms.BindingSource(this.components);
            this.OutputBunding = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.InputBinding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutputBunding)).BeginInit();
            reportDataSource1.Name = "dsInputDataReport";
            reportDataSource1.Value = this.InputBinding;
            reportDataSource2.Name = "dsInputDataOutput";
            reportDataSource2.Value = this.OutputBunding;
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer.LocalReport.ReportEmbeddedResource = "SDT_AP_WPFframework.AirEmissionReport.rdlc";
           
          
            ((System.ComponentModel.ISupportInitialize)(this.InputBinding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutputBunding)).EndInit();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            this.ReportViewer.RefreshReport();
        }
    }
}
