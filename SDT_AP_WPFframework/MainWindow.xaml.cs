﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using AirPollutionLib; 

namespace SDT_AP_WPFframework
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Report frmRpt;
        List<ForReport> RepListInput = new List<ForReport>();
       
        ClassAirPollutionInput AirPollutionInputBase = new ClassAirPollutionInput(/*80, 6.4, 100, 30, 0.1, 1198800, 160, 75*/);
        ClassAirPollutionCount AirPollutionCount = new ClassAirPollutionCount();

        //Работа с Toggle
        #region toggles
        //получение таглов формы (для скоростей)
        ToggleButton[] GetToggle(MainWindow mw) 
        {
            List<ToggleButton> Buttons = new List<ToggleButton>();
            FieldInfo[] FIELDS = mw.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo info in FIELDS)
            {
                if (info.FieldType == typeof(ToggleButton))
                {
                    Buttons.Add((ToggleButton)info.GetValue(mw));
                }
            }
            return Buttons.ToArray();

        }          

        //метод переключения таглов
        public void  SwitchToggle(object sender, EventArgs e)
        {
            ToggleButton tb = sender as ToggleButton;
            if (tb != null)
            {

                    foreach (ToggleButton t in GetToggle(this))

                    {
                        t.IsChecked = false;
                        
                    }
                    tb.IsChecked = true;
                // Присваивание скорости,необходимой для финальных расчетов
                double.TryParse(tb.Content.ToString(), out AirPollutionInputBase.Velocity);
                
            }
        }
        #endregion

        //Получение вида пыли для расчетов
        public void GetE(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            { 
              

                if (rb.Content.ToString() == "Пыль")
                {
                    AirPollutionInputBase.E = 0;
                }

                if (rb.Content.ToString() == "Аэрозоль")
                {
                    AirPollutionInputBase.E = 1;
                }

              
            }
        }

        //Ввод символов в боксы
        #region textboxvalid
        public int DS_Count(string s)
        {
            string substr = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString();
            int count = (s.Length - s.Replace(substr, "").Length) / substr.Length;
            return count;
        }

        public void TextboxValidation(object sender, TextCompositionEventArgs e) 
        {
            foreach (TextBox tx in InputGrid.Children.OfType<TextBox>())
            {
                if (tx.IsKeyboardFocused)
                {
                    if (tx.Name == "TRbox" | tx.Name == "Tabox" | tx.Name == "nubox" | tx.Name == "Abox")
                    {

                        e.Handled = !((Char.IsDigit(e.Text, 0) || ((e.Text == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()) && (DS_Count(((TextBox)sender).Text) < 0))));

                    }
                    else
                    {
                        e.Handled = !((Char.IsDigit(e.Text, 0) || ((e.Text == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()) && (DS_Count(((TextBox)sender).Text) < 1))));

                    }
                }
                
            }
      
            
              
            
        }
        #endregion


        /// <summary>
        /// Создание отчета.Занесение данных в список с последующей его передачей в Биндинги.Вроде бы всё просто
        /// </summary>
        public void CreateReport()
        {
            frmRpt = new Report();

            List<ForReport> RepListInput = new List<ForReport>();

            RepListInput.Add(new ForReport("Высота источника выброса,м", AirPollutionInputBase.H.ToString()));
            RepListInput.Add(new ForReport("Диаметр устья источника выброса,м", AirPollutionInputBase.D.ToString()));
            RepListInput.Add(new ForReport("Температура выброса на уровне устья,°С", AirPollutionInputBase.Tr.ToString()));
            RepListInput.Add(new ForReport("Cредняя температура атмосферного воздуха в районе,°С", AirPollutionInputBase.Ta.ToString()));
            RepListInput.Add(new ForReport("Концентрация вредного вещества в выбросе,мг/м3", AirPollutionInputBase.Zo.ToString()));
            RepListInput.Add(new ForReport("Объем выброса,м3/ч", AirPollutionInputBase.V.ToString()));
            RepListInput.Add(new ForReport("Коэффициент эффективности очистки выброса от вредных веществ,%", AirPollutionInputBase.nu.ToString()));
            RepListInput.Add(new ForReport("Коэффициент температурной стратификации атмосферы,с2/3мг.град 1/3", AirPollutionInputBase.A.ToString()));
            if (AirPollutionInputBase.E == 0) 
            {
                RepListInput.Add(new ForReport("Вид вредного выброса", "Пыль" ));
            }
            if (AirPollutionInputBase.E == 1) 
            {
                RepListInput.Add(new ForReport("Вид вредного выброса", "Аэрозоль"));
            }

             RepListInput.Add(new ForReport("Скорость ветра,м/с", AirPollutionInputBase.Velocity.ToString()));

            List<ForReport> RepListOutPut = new List<ForReport>();
            RepListOutPut.Add(new ForReport("Средняя скорость в устье,м/с", AirPollutionCount.Wo_AverageVelocityOutfall().ToString()));
            RepListOutPut.Add(new ForReport("Количество пыли, г/с", AirPollutionCount.M_DustAmount().ToString()));
            RepListOutPut.Add(new ForReport("Объем газовоздушной смеси, м3/с", AirPollutionCount.V1_MixtureVolume().ToString()));
            RepListOutPut.Add(new ForReport("Максимальная приземная концентрация,г/м3 ", AirPollutionCount.Cm_MaxPollutionConcentration().ToString()));
            RepListOutPut.Add(new ForReport("На расстоянии,м", AirPollutionCount.Xm_TorchDistance().ToString()));
            RepListOutPut.Add(new ForReport("Опасная скорость ветра у устья источника выброса, м/с", AirPollutionCount.um_DangerousWindVelocity().ToString()));
            for (int i = 0; i < 5; i++) 
            {
                RepListOutPut.Add(new ForReport($"Приземная концентрация на расстоянии {AirPollutionCount.X[i].ToString()} метров,г/м3 ", AirPollutionCount.Cx_ConcentrationCountOfDistanceCount()[i].ToString()));
            }




            frmRpt.InputBinding.DataSource = RepListInput;
            frmRpt.OutputBunding.DataSource =RepListOutPut;
            frmRpt.Show();


        }

        public MainWindow()
        {
            InitializeComponent();

            #region Методы для взаимодействия элементов интерфейса с данными
            foreach (ToggleButton tb in GetToggle(this))
            {
                 tb.Click +=  SwitchToggle;
                

            }
            foreach (RadioButton rb in InputGrid.Children.OfType<RadioButton>()) 
            {
                rb.Click +=   GetE;
            }

            foreach (TextBox rb in InputGrid.Children.OfType<TextBox>())    
            {
                rb.PreviewTextInput += TextboxValidation;
            }
            #endregion

            //Сериализация
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(ClassAirPollutionInput));
                using (FileStream fs = new FileStream("InputData.txt", FileMode.OpenOrCreate)) 
                {

                    AirPollutionInputBase = (ClassAirPollutionInput)formatter.Deserialize(fs);
                    
                }
                Hbox.Text = AirPollutionInputBase.H.ToString();
                Dbox.Text = AirPollutionInputBase.D.ToString();
                TRbox.Text = AirPollutionInputBase.Tr.ToString();
                Tabox.Text = AirPollutionInputBase.Ta.ToString();
                Zobox.Text = AirPollutionInputBase.Zo.ToString();
                nubox.Text = AirPollutionInputBase.nu.ToString();
                V1box.Text = AirPollutionInputBase.V.ToString();
                Abox.Text = AirPollutionInputBase.A.ToString();
            }
            catch 
            {
              
            }

        }
        ToggleButton[] toggleButtons{ get { return GetToggle(this); } }



        //вызов справки
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Help help = new Help();
            help.Show();
        }
        //Кнопка Calcutale
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (TextBox tx in InputGrid.Children.OfType<TextBox>())
            {
                if (tx.Text.ToString() == string.Empty)
                {
                    MessageBox.Show("Необходимо заполнить все данные", "ошибОчка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
               
            }
            #region ПроверОчки
            //проверка на выбор скорости
            int f = 0;
            foreach (ToggleButton tg in GetToggle(this))
            {
                if ((bool)tg.IsChecked)
                {

                    f++;
                    break;
                }
            }
            if (f == 0)
            {
                MessageBox.Show("Выберите скорость", "ошибОчка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            //проверка на вбор вида
            int m = 0;
            foreach (RadioButton rb in InputGrid.Children.OfType<RadioButton>())
            {
                if ((bool)rb.IsChecked)
                {
                    m++;
                }
            }
            if (m == 0)
            {
                MessageBox.Show("Выберите вид вредного выброса", "ошибОчка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            #endregion
            //Передача данных из окна в констуртор класса
            ClassAirPollutionInput AirPollutionInput = new ClassAirPollutionInput(Convert.ToDouble(Hbox.Text.ToString()), Convert.ToDouble(Dbox.Text.ToString()), Convert.ToInt32(TRbox.Text.ToString()), Convert.ToInt32(Tabox.Text.ToString()), Convert.ToDouble(Zobox.Text.ToString()), Convert.ToDouble(V1box.Text.ToString()), Convert.ToInt32(Abox.Text.ToString()), Convert.ToInt32(nubox.Text.ToString()));

            AirPollutionInput.E = AirPollutionInputBase.E;
            AirPollutionInput.Velocity = AirPollutionInputBase.Velocity;

            AirPollutionCount.AirPollutionInput = AirPollutionInput;
            int k = 0;
            int j = 0;
            // Вывод финальных данных на форму
            foreach (var info in GridOut.Children.OfType<TextBox>())
            {
                if (j < 5)
                {
                    if (Double.IsNaN(AirPollutionCount.Cy_ConcentrationCountOfDistanceCount()[0, 0]))
                    {
                        MessageBox.Show("Вы получили не число.Исправьте входные данные", "Не число", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    }
                    info.Text = AirPollutionCount.Cy_ConcentrationCountOfDistanceCount()[k, j].ToString();
                    DustAmount.Text = AirPollutionCount.M_DustAmount().ToString();
                    DangerWindVelocity.Text = AirPollutionCount.um_DangerousWindVelocity().ToString();
                    GraphButton.IsEnabled = true;
                    ReportButton.IsEnabled = true;


                    if (k < 4)
                        k++;
                    else
                    {
                        j++;
                        k = 0;
                    }
                }
                

            }
            int o = 0;

            foreach (TextBox tx in Inside2.Children.OfType<TextBox>())
            {
                if (Double.IsNaN(AirPollutionCount.Cy_ConcentrationCountOfDistanceCount()[0, 0]))
                {
                  
                    break;
                }
                if (o < 5)
                {
                    tx.Text = AirPollutionCount.Cx_ConcentrationCountOfDistanceCount()[o].ToString();
                }
                if (o < 4)
                {
                    o++;
                }
                else 
                {
                    o = 0;
                }
            }
            
            #region Serialize
            AirPollutionInputBase.H = AirPollutionInput.H;
            AirPollutionInputBase.D = AirPollutionInput.D;
            AirPollutionInputBase.Tr = AirPollutionInput.Tr;
            AirPollutionInputBase.Ta = AirPollutionInput.Ta;
            AirPollutionInputBase.Zo = AirPollutionInput.Zo;
            AirPollutionInputBase.V = AirPollutionInput.V;
            AirPollutionInputBase.A = AirPollutionInput.A;
            AirPollutionInputBase.nu = AirPollutionInput.nu;

            XmlSerializer formatter = new XmlSerializer(typeof(ClassAirPollutionInput));
            using (FileStream fs = new FileStream("InputData.txt", FileMode.Create))
            {
                formatter.Serialize(fs, AirPollutionInputBase);
            }
            //Сделать кнопки активными после нажатия на рассчитать
           
            #endregion
        }
        //Открытие графа
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
           
                Graph gr = new Graph(AirPollutionCount);
                gr.Show();
            
        }
        //Открытие отчета
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            CreateReport();
            


        }

        
    }
}
